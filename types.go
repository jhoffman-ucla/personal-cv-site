package main

import (
	"fmt"
	log "github.com/sirupsen/logrus"
	//"gopkg.in/yaml.v3"
	"html/template"
	"strings"
)

type Author struct {
	First string
	Last  string
}

type ListBox struct {
	Title string
	Items map[string]HTMLProducer
}

type WebsiteList struct {
	Title string
	Items map[string]*Website
}

type JobList struct {
	Title string
	Items map[string]*Job
}

type DegreeList struct {
	Title string
	Items map[string]*Degree
}

type AbstractList struct {
	Title string
	Items map[string]*Abstract
}

type PublicationList struct {
	Title string
	Items map[string]*Publication
}

type HTMLProducer interface {
	ToHTML() template.HTML
}

type PersonalInfo struct {
	FirstName         string `form:"label=first name"`
	LastName          string `form:"label=last name"`
	Email             string `form:"label=email"`
	ResearchInterests string `form:"label=research interests"`
	ProfilePhoto      string `form:"label=research interests"`
}

type Landing struct {
	Title    string `form:"label=title"`
	Message  string `form:"label=message"`
	Initials string `form:"label=intials"`
	Date     string `form:"label=date"`
}

type Website struct {
	Title string `form:"label=link title"`
	HREF  string `form:"label=link href"`
}

func (w Website) ToHTML() template.HTML {
	return template.HTML(fmt.Sprintf("<a href=\"%s\">%s</a>", w.HREF, w.Title))
}

type DegreeType string

const (
	DegreePhD DegreeType = "Ph.D."
	DegreeBS  DegreeType = "B.S."
	DegreeBA  DegreeType = "B.A."
	DegreeMS  DegreeType = "M.S."
)

type Degree struct {
	Type       DegreeType `form:"label=degree type"`
	Major      string     `form:"label=major"`
	University string     `form:"label=university"`
	City       string     `form:"label=city"`
	State      string     `form:"label=state"`
	Year       int        `form:"label=graduation year"`
	LinkTitle  string     `form:"label=link title"`
	LinkHREF   string     `form:"label=link href"`
}

func (d Degree) ToHTML() template.HTML {
	tpl := template.Must(template.New("").Parse("{{.Type}} {{.Major}}, {{.University}}, {{.City}}, {{.State}} {{if .LinkTitle}}(<a href=\"{{.LinkHREF}}\">{{.LinkTitle}}</a>){{end}}"))
	b := &strings.Builder{}
	tpl.Execute(b, d)
	return template.HTML(b.String())
}

type Job struct {
	Title     string `form:"label=psoition title"`
	Company   string `form:"label=company name"`
	StartDate string `form:"label=start date"`
	EndDate   string `form:"label=end date"`
}

func (j Job) ToHTML() template.HTML {
	tpl := template.Must(template.New("").Parse("{{.Title}}, {{.Company}}. {{.StartDate}}, {{.EndDate}}"))
	b := &strings.Builder{}
	tpl.Execute(b, j)
	return template.HTML(b.String())
}

type Publication struct {
	LinkHREF  string
	PDFHREF   string
	VideoHREF string

	State string

	Title   string
	Authors []Author
	Journal string
	Year    int
	Volume  int
	Issue   int
	Pages   string
}

func (p Publication) ToHTML() template.HTML {
	links_block := `{{with .LinkHREF}}(<a href="{{.}}">link</a>){{end}}{{with .PDFHREF}}(<a href="{{.}}">pdf</a>){{end}}{{with .VideoHREF}}(<a href="{{.}}">video</a>){{end}}`

	tpl := template.Must(template.New("").Parse(links_block + " {{with .Authors}}{{range .}}{{.First}} {{.Last}}, {{end}}{{end}}<b>{{.Title}}</b>. {{.Journal}} {{.Volume}}({{.Issue}}), {{.Pages}}. ({{.Year}})"))
	b := &strings.Builder{}
	tpl.Execute(b, p)
	return template.HTML(b.String())
}

type Abstract struct {
	LinkHREF        string
	SlidesHREF      string
	PosterHREF      string
	ProceedingsHREF string
	VideoHREF       string

	Title      string
	Authors    []Author
	Conference string
	Location   string
	Date       string
}

func (a Abstract) ToHTML() template.HTML {

	links_block := `{{with .LinkHREF}}(<a href="{{.}}">link</a>){{end}}{{with .SlidesHREF}}(<a href="{{.}}">slides</a>){{end}}{{with .PosterHREF}}(<a href="{{.}}">poster</a>){{end}}{{with .ProceedingsHREF}}(<a href="{{.}}">proceedings</a>){{end}}{{with .VideoHREF}}(<a href="{{.}}">video</a>){{end}}`

	tpl, err := template.New("").Parse(links_block + " {{with .Authors}}{{range .}}{{.First}} {{.Last}}, {{end}}{{end}}<b>{{.Title}}</b>. {{.Conference}}, {{.Location}}, ({{.Date}})")
	if err != nil {
		log.Fatal("failed to parse: ", err)
	}

	b := &strings.Builder{}
	tpl.Execute(b, a)
	return template.HTML(b.String())
}
