package main

import (
	"context"
	log "github.com/sirupsen/logrus"
	"net/http"
	"time"
)

func LoggingMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		start := time.Now()

		next.ServeHTTP(w, r)

		elapsed := time.Since(start)
		log.WithFields(log.Fields{
			"url":      r.URL,
			"method":   r.Method,
			"protocol": r.Proto,
			"remote":   r.RemoteAddr,
		}).Infof("Elapsed: %v", elapsed)

	})
}

func AuthMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		session_cookie, err := r.Cookie("session_token")
		if err != nil {
			http.Redirect(w, r, "/", http.StatusNotFound)
			return
		}

		if !sessions.IsValid(session_cookie.Value) {
			http.Redirect(w, r, "/", http.StatusNotFound)
			return
		}

		ctx := r.Context()
		ctx = context.WithValue(ctx, "session_valid", true)

		r = r.WithContext(ctx)
		next.ServeHTTP(w, r)

	})
}
