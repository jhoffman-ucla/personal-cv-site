package main

import (
	_ "embed"
	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
	"net/http"
	"os"
	"path/filepath"
)

// Templates

//go:embed templates/index.tpl
var index_tpl string

//go:embed templates/update.tpl
var update_tpl string

//go:embed templates/login.tpl
var login_tpl string

//go:embed templates/about.tpl
var about_tpl string

//go:embed templates/nuggets.tpl
var nuggets_tpl string

// Stuff that should eventually move to a configuration
var data_dir = "data"
var static_dir = filepath.Join(data_dir, "static")
var media_dir = filepath.Join(data_dir, "media")

var user = os.Getenv("USER")
var pass = os.Getenv("PASS")

var sessions *SessionStore = &SessionStore{}

var curr_cv *CV = &CV{}

func main() {

	err := curr_cv.Load()
	if err != nil {
		log.Warn("no existing db found:", err)

		curr_cv = DefaultCV()
		//curr_cv.Save()
	}

	r := mux.NewRouter()
	r.Use(LoggingMiddleware)

	// Public endpoints
	r.HandleFunc("/", IndexHandler)
	r.HandleFunc("/about", AboutHandler)
	r.HandleFunc("/nuggets", NuggetsHandler)
	r.HandleFunc("/login", LoginHandler)

	fs_static := http.FileServer(http.Dir(static_dir))
	r.PathPrefix("/static/").Handler(http.StripPrefix("/static/", fs_static))

	fs_media := http.FileServer(http.Dir(media_dir))
	r.PathPrefix("/media/").Handler(http.StripPrefix("/media/", fs_media))

	// Protected endpoints
	r_prot := r.PathPrefix("/_/").Subrouter()
	r_prot.Use(AuthMiddleware)

	r_prot.HandleFunc("/publication/{id}", PublicationHandler)
	r_prot.HandleFunc("/abstract/{id}", AbstractHandler)
	r_prot.HandleFunc("/job/{id}", JobHandler)
	r_prot.HandleFunc("/website/{id}", WebsiteHandler)
	r_prot.HandleFunc("/save", SaveHandler)

	log.Fatal(http.ListenAndServe(":8080", r))
}
