package main

import (
	"github.com/google/uuid"
	"time"
)

type Session struct {
	Token  string
	Expiry time.Time
}

func NewSession() *Session {
	return &Session{
		Token:  uuid.New().String(),
		Expiry: time.Now().Add(12 * time.Hour),
	}
}

type SessionStore map[string]*Session

func (sess SessionStore) IsValid(token string) bool {

	s, ok := sess[token]
	if !ok {
		return false
	}

	if time.Now().After(s.Expiry) {
		delete(sess, s.Token)
		return false
	}

	return true
}

func (sess SessionStore) Add(s *Session) {
	sess[s.Token] = s
	return
}
