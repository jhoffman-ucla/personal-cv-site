<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    </head>
    <body class="d-flex flex-column min-vh-100">
        <div class="container">
            <form method="post" action="/_/{{.Type}}/{{.ID}}">
                <h1>Update {{.Type}}</h1>
                    {{inputs_for .Item}}
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </body>
</html>