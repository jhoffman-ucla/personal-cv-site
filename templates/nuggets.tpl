<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    </head>
    <body class="d-flex flex-column min-vh-100">
        <div class="container">
            <p style="text-align:center;"><a href="/">home</a></p>
            <hr>
            <h1 style="text-align:center;">"Nuggets"</h1>

            <p>A mentor of mine, Royce Zia, has a series of 1-2 slide presentations he put together for the NSF that he referred to as "nuggets." The purpose of these was to playfully and briefly discuss and explore a topic of ongoing research (in Royce's case "Statistical Mechanics Far from Equilibrium") and how students were playing a role in ongoing research. It has become completely entrenched in my brain to describe these sort of short-form, pseudo-informal academic writings as "nuggets."</p>

            <p>That's the idea here: for me to set aside time once per week to playfully and briefly talk about a more academic topic. I must write at least one paragraph but can spend no more than 30 minutes writing; 5000 character maximum (~6 paragraphs). I write down one idea per day throughout the week and then select the topic when it's time to write. If I carry an idea forward between weeks it counts against my "idea allotment" so that I'm never trying to pick from more than seven at a time at a time.</p>

            <p>My hope is that by regularly reflecting on these things I'll start to build some longer-term threads into some interesting topics.</p>

            <p><b>Nuggets is temporarily unavailable as I migrate my data from the old database into the new site format.  Part of the purpose of the site rewrite is to make it easier to write these posts in the future!</b></p>
            <p style="text-align:right;">John - July 16, 2023</p>

        </div>
    </body>
</html>
