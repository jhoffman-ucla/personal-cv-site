<!-- TITLE TEMPLATE -->
{{define "title"}}
<h1 class="text-center">{{.FirstName}} {{.LastName}}</h1>
<img src="{{.ProfilePhoto}}" class="mx-auto d-block img-fluid" alt="profile photo of john speaking at a jack and floramae's wedding"></img>
<br>
<p class="text-center">{{.Email}}</p>
{{end}}

<!-- LANDING TEMPLATE -->
{{define "landing"}}
<h2>{{.Title}}</h2>
<p>{{.Message}}</p>
<p style="text-align: right;">{{.Initials}} - {{.Date}}</p>
{{end}}

<!-- LISTBOX TEMPLATE -->
{{define "list-box"}}
<h2>{{.Title}}</h2>
<ul>
{{range .Items}}
    <li>{{.ToHTML}}</li> 
    {{end}}
</ul>
{{end}}

<!-- RESEARCH INTERESTS TEMPLATE -->
{{define "research-interests"}}
<h2>Research Interests</h2>
<p>{{.}}</p>
{{end}}

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    </head>
    <body class="d-flex flex-column min-vh-100">
        <div class="container">
            <hr>
            {{template "title" .PersonalInfo }}
            <hr>
            {{template "landing" .Landing}}
            <hr>
            {{template "list-box" .Websites}}
            <hr>
            {{template "research-interests" .PersonalInfo.ResearchInterests}}
            <hr>
            {{template "list-box" .Degrees}}
            <hr>
            {{template "list-box" .Jobs}}
            <hr>
            {{template "list-box" .Publications}}
            <hr>
            {{template "list-box" .Abstracts}}
            <hr>
        </div>
    </body>
</html>
