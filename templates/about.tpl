<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    </head>
    <body class="d-flex flex-column min-vh-100">
        <div class="container">
            <p style="text-align:center;"><a href="/">home</a></p>
            <hr>
            <h1 style="text-align:center;">About this site</h1>

            <p>Building and running this site is a hobby project of mine that I do in my spare time.  In addition to developing all of the site software, I also run all of the networking and server equipment out of my apartment.  High risk, low reward? A little, but I enjoy wrangling my own little corner of the internet.</p>

            <img src="/static/rack-photo.jpg" class="mx-auto d-block img-fluid" max-width="50" style="max-width: 40%;" alt="photograph of john's server rack with the webserver labeled with a red x and a (poorly drawn) 'you are here!' sign">
            
            <p>Some info:</p>
            <ul>
                <li>Site is 100% <a href="https://go.dev">Go</a></li>
                <li>Previous version was <a href="https://www.djangoproject.com/">Django</a>, but I found it slow and cumbersome, and WSGI an annoying complication.  Django was just way overkill for what I'm interested in maintaining.</li>
                <li>Styling is essentially minimum bootstrap to get decent behavior on different devices</li>
                <li>I'm thinking about turning this project into a "white-labelable" site for folks in academia - <a href="mailto:johnmarianhoffman@gmail.com">interested?</a></li>
            </ul>

            <p>This website and the links on the home page are the only web presences I maintain.  Although they are not <em>broadly</em> intended for the public (mostly friends and family), I also have an <a href="https://www.instagram.com/jmh_woodworks/">instagram</a>, a few videos on <a href="https://www.youtube.com/@johnmarianhoffman">youtube</a>, and a largely inactive <a href="https://www.linkedin.com/in/john-hoffman-77103b11">linkedin</a>.</p>  

            <p>I mention the above for two reasons: (1) in an era of fake accounts, and AI generated whatnot, etc. these are the <em>only</em> online places to find me (if you see "me" somewhere else other than the above, please let me know); and (2) although you are welcome to connect with me just about anywhere, it's 2023 and many of us maintain fairly separate personal/professional lives online so please use your best judgement about whether or not you wish to follow me in different online spaces.  Not everything I share online is meant for everyone.</p>

            <p>Thanks for taking the time to click this link and I'd love to hear from you!  Any questions or comments can be directed to <a href="mailto:johnmarianhoffman@gmail.com">johnmarianhoffman@gmail.com</a>.</p>

            <p style="text-align:right;">John - July 17, 2023</p>

        </div>
    </body>
</html>
