FROM golang:alpine as builder

RUN mkdir /app
WORKDIR /app

COPY go.mod /app/
COPY go.sum /app/
COPY *.go /app/
COPY templates /app/templates
RUN go mod tidy

RUN go build -o personal-site

FROM alpine as runner
COPY --from=builder /app/personal-site /personal-site
ENTRYPOINT ["/personal-site"]