package main

import (
	"github.com/gorilla/mux"
	"github.com/gorilla/schema"
	"github.com/joncalhoun/form"
	log "github.com/sirupsen/logrus"
	"html/template"
	"net/http"
)

func LoginHandler(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodGet:
		tpl, err := template.New("login").Parse(login_tpl)
		if err != nil {
			log.Fatal("failed to parse template: ", err)
		}

		err = tpl.Execute(w, nil)
		if err != nil {
			log.Error("failed to execute template: ", err)
		}
	case http.MethodPost:
		r.ParseMultipartForm(4096)

		if r.FormValue("username") == user && r.FormValue("password") == pass {
			// Generate token, set cookie, save to sessions
			s := NewSession()
			sessions.Add(s)

			cookie := &http.Cookie{Name: "session_token", Value: s.Token, Expires: s.Expiry}
			http.SetCookie(w, cookie)

			log.Info("authentication success!")
		}

		http.Redirect(w, r, "/", http.StatusFound)
	}
}

func AboutHandler(w http.ResponseWriter, r *http.Request) {
	tpl, err := template.New("about").Parse(about_tpl)
	if err != nil {
		log.Fatal("failed to parse template: ", err)
	}

	err = tpl.Execute(w, nil)
	if err != nil {
		log.Error("failed to execute template: ", err)
	}
}

func NuggetsHandler(w http.ResponseWriter, r *http.Request) {
	tpl, err := template.New("nuggets").Parse(nuggets_tpl)
	if err != nil {
		log.Fatal("failed to parse template: ", err)
	}

	err = tpl.Execute(w, nil)
	if err != nil {
		log.Error("failed to execute template: ", err)
	}
}

func SaveHandler(w http.ResponseWriter, r *http.Request) {
	err := curr_cv.Save()
	if err != nil {
		log.Error("failed to save db:", err)
	}
}

func IndexHandler(w http.ResponseWriter, r *http.Request) {
	tpl, err := template.New("index").Parse(index_tpl)
	if err != nil {
		log.Fatal("failed to parse template: ", err)
	}

	err = tpl.Execute(w, curr_cv)
	if err != nil {
		log.Error("failed to execute template: ", err)
	}
}

var input_tpl = `
<div class="form-group">
<label {{with .ID}}for="{{.}}"{{end}}>
	            {{.Label}}
            </label>
            <input class="form-control" {{with .ID}}id="{{.}}"{{end}} type="{{.Type}}" name="{{.Name}}" placeholder="{{.Placeholder}}" {{with .Value}}value="{{.}}"{{end}}>
</div>
`

func PublicationHandler(w http.ResponseWriter, r *http.Request) {
	UpdateHandler("publication", w, r)
}

func AbstractHandler(w http.ResponseWriter, r *http.Request) {
	UpdateHandler("abstract", w, r)
}

func JobHandler(w http.ResponseWriter, r *http.Request) {
	UpdateHandler("job", w, r)
}

func WebsiteHandler(w http.ResponseWriter, r *http.Request) {
	UpdateHandler("website", w, r)
}

func UpdateHandler(resource_type string, w http.ResponseWriter, r *http.Request) {

	// Check for id
	params := mux.Vars(r)
	id, ok := params["id"]
	if !ok {
		log.Error("")
	}

	// Grab current item
	var curr_item interface{}

	switch resource_type {
	case "abstract":
		curr_item = curr_cv.Abstracts.Items[id]
	case "publication":
		curr_item = curr_cv.Publications.Items[id]
	case "job":
		curr_item = curr_cv.Jobs.Items[id]
	case "website":
		curr_item = curr_cv.Websites.Items[id]
	}

	if curr_item == nil {
		switch resource_type {
		case "abstract":
			curr_item = &Abstract{}
		case "publication":
			curr_item = &Publication{}
		case "job":
			curr_item = &Job{}
		case "website":
			curr_item = &Website{}
		}
	}

	switch r.Method {
	case http.MethodPost:
		r.ParseForm()
		dec := schema.NewDecoder()
		dec.IgnoreUnknownKeys(true)

		err := dec.Decode(curr_item, r.PostForm)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		switch resource_type {
		case "abstract":
			curr_cv.Abstracts.Items[id] = curr_item.(*Abstract)
		case "publication":
			curr_cv.Publications.Items[id] = curr_item.(*Publication)
		case "job":
			curr_cv.Jobs.Items[id] = curr_item.(*Job)
		case "website":
			curr_cv.Websites.Items[id] = curr_item.(*Website)
		}

		err = curr_cv.Save()
		if err != nil {
			log.Error("during record update, failed to save db:", err)
		}

		fallthrough
	case http.MethodGet:

		tpl_input := template.Must(template.New("").Parse(input_tpl))

		fb := form.Builder{
			InputTemplate: tpl_input,
		}

		tpl, err := template.New("update").Funcs(fb.FuncMap()).Parse(update_tpl)
		if err != nil {
			log.Fatal("failed to parse template: ", err)
		}

		data := struct {
			ID   string
			Type string
			Item interface{}
		}{
			ID:   id,
			Type: resource_type,
			Item: curr_item,
		}

		err = tpl.Execute(w, data)
		if err != nil {
			log.Error("failed to execute template: ", err)
		}
	case http.MethodDelete:
		switch resource_type {
		case "abstract":
			delete(curr_cv.Abstracts.Items, id)
		case "publication":
			delete(curr_cv.Publications.Items, id)
		case "job":
			delete(curr_cv.Jobs.Items, id)
		case "website":
			delete(curr_cv.Websites.Items, id)
		}

		err := curr_cv.Save()
		if err != nil {
			log.Error("during delete, failed to save db:", err)
		}
	}

}
